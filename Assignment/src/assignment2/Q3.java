package assignment2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Q3 {

	public static String convertTime(String input)
	        throws ParseException
	    {

	        DateFormat dateFormat
	            = new SimpleDateFormat("hh:mm:ss aa");

	        DateFormat format
	            = new SimpleDateFormat("HH:mm:ss");
	        Date time = null;
	        String output = "";
	       
	        time = dateFormat.parse(input);

	        output = format.format(time);
	        return output;
	    }
	
	public static void main(String[] args) 
			
	throws ParseException{
		
		Scanner s = new Scanner(System.in);
		String x = s.nextLine();
		
			
	        System.out.println(convertTime(x));
	}

}
