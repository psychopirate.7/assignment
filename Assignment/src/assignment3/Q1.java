package assignment3;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		int ticket;
		float cost;
		String refreshment,coupon,circle;
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the no of ticket:");
		ticket=sc.nextInt();
		sc.nextLine();
		
		
		
		if(ticket<5||ticket>40) {
			System.out.println("Minimum of 5 and Maximum of 40 Tickets");
		}else {
			System.out.print("Do you want refreshment:");
			refreshment=sc.nextLine();

			
			System.out.print("Do you have coupon code:");
			coupon=sc.nextLine();

			System.out.print("Enter the circle:");
			circle=sc.nextLine();
			sc.close();
			
			if(!circle.equalsIgnoreCase("k") && !circle.equalsIgnoreCase("q")) {
				System.out.println("Invalid Input"); 
				System.out.println(circle+coupon+refreshment);
			}else {
				if(ticket<20) {
					if(circle.equalsIgnoreCase("k")) {
						if(coupon.equalsIgnoreCase("y")) {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=((ticket*75)*0.98f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*75)*0.98f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}else {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=(ticket*75)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*75);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}
					}else {
						if(coupon.equalsIgnoreCase("y")) {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=((ticket*150)*0.98f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*150)*0.98f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}else {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=(ticket*150)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*150);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
							
						}
						
					}
					
				}else {
					if(circle.equalsIgnoreCase("k")) {
						if(coupon.equalsIgnoreCase("y")) {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=(((ticket*75)*0.9f)*0.98f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=((ticket*75)*0.9f)*0.98f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}else {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=((ticket*75)*0.9f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*75)*0.9f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}
					}else {
						if(coupon.equalsIgnoreCase("y")) {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=(((ticket*150)*0.9f)*0.98f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=((ticket*150)*0.9f)*0.98f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}
						}else {
							if(refreshment.equalsIgnoreCase("y")) {
								cost=((ticket*150)*0.9f)+(ticket*50);
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
							}else {
								cost=(ticket*150)*0.9f;
								System.out.println("Ticket cost:"+String.format("%.2f", cost));
								
							}
							
						}
					
				}
			}
		}
		
		
	}
	}
}
