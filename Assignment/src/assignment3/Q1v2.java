package assignment3;

import java.util.Scanner;

public class Q1v2 {

	static int numOfTicket1;
	static double totalCost, refreshmentCost, totalRefreshmentCost, couponCodeDiscount, totalCouponCodeDiscount, extraDiscount; 
	public static void main(String[] args) {
		//public static void main(String[] args) {
			
			
		//	char refreshment, couponCode, typeOfCircle;
			//static double totalCost, refreshmentCost, totalRefreshmentCost, couponCodeDiscount, totalCouponCodeDiscount, extraDiscount; 
			
			Scanner scan = new Scanner(System.in); 
			
			System.out.println("Enter number of ticket");
			numOfTicket1= scan.nextInt(); 
			
			if (numOfTicket1>= 5 && numOfTicket1<=40) {
				
				if (numOfTicket1>=20 && numOfTicket1<=40) {
					
					extraDiscount = 0.90; 
					
					System.out.println("Do you want refreshment? Enter Y if yes, N if no: ");
					char refreshment = scan.next().charAt(0); 
					
					if(refreshment == 'Y') {
					    refreshmentCost = 50; 
					    totalRefreshmentCost = refreshmentCost*numOfTicket1; 
					} else if  (refreshment == 'N') { 
						totalRefreshmentCost = 0; 	
					}
					
				   System.out.println("Do you have coupon? Enter Y if yes, N if no: ");
				   char couponCode = scan.next().charAt(0); 
				   
				   if(couponCode == 'Y') {
					    couponCodeDiscount = 0.98; 
					    totalCouponCodeDiscount = 0.98; 
				   } else if  (couponCode == 'N') { 
						totalCouponCodeDiscount = 0; 	
				   }   	   
			 } else {
						System.out.println("Do you want refreshment? Enter Y if yes, N if no: ");
						char refreshment = scan.next().charAt(0); 
					
						if(refreshment == 'Y') {
							refreshmentCost = 50; 
							totalRefreshmentCost = refreshmentCost*numOfTicket1; 
						} else if  (refreshment == 'N') { 
							totalRefreshmentCost = 0; 	
						}
			 	 }
				
			 System.out.println("Enter the circle. Enter K if king, Q if queen");
			 char typeOfCircle = scan.next().charAt(0);
			 
			 if (typeOfCircle == 'K') {
				 totalCost = ((75*numOfTicket1)*(extraDiscount*totalCouponCodeDiscount))+totalRefreshmentCost; 
				 System.out.println("Ticket cost: " +totalCost);
			 }  else if (typeOfCircle == 'Q') {
				 totalCost = ((150*numOfTicket1)+totalRefreshmentCost)*(extraDiscount+totalCouponCodeDiscount); 
				 System.out.println("Ticket cost: " +totalCost);
			 } else {
				 System.out.println("Invalid input");
			 }
			 
			} else {
			  System.out.println("Minimum of 5 and Maximum of 40 Tickets");
			}
		}

	
	}