package excersice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeConvert {
	
	public static void main(String[] args) throws ParseException{
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter 12 hour format time");
		String x=s.nextLine();
		
		DateFormat format = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatout = new SimpleDateFormat("HH:mm:ss");
		
		Date date=null;
		date=format.parse(x);

		if(date!=null) {
			String myDate = formatout.format(date);
			System.out.println(myDate);
		}
		s.close();
	}
}
